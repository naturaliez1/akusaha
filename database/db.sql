/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.14-MariaDB : Database - apdonasi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`apdonasi` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `apdonasi`;

/*Table structure for table `dtmodules` */

DROP TABLE IF EXISTS `dtmodules`;

CREATE TABLE `dtmodules` (
  `idgroup` char(15) NOT NULL,
  `idmod` char(15) NOT NULL,
  PRIMARY KEY (`idgroup`,`idmod`),
  KEY `FK_dtmodules1` (`idmod`),
  CONSTRAINT `FK_dtmodules0` FOREIGN KEY (`idgroup`) REFERENCES `groups` (`idgroup`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dtmodules1` FOREIGN KEY (`idmod`) REFERENCES `modules` (`idmod`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dtmodules` */

insert  into `dtmodules`(`idgroup`,`idmod`) values 
('G20180920011911','M20180920031131'),
('G20180920011911','M20180920031531'),
('G20180920011911','M20180920031644'),
('G20180920011911','M20180920031751'),
('G20180920011911','M20180920032009'),
('G20180920011911','M20181222032707'),
('G20180920011911','M20190817051952'),
('G20180920011911','M20191013080517'),
('G20180920011911','M20191017023845'),
('G20180920011911','M20191017023846'),
('G20180920011911','M20191018064607'),
('G20180920011911','M20191018073128'),
('G20180920011911','M20191018073146'),
('G20180920011911','M20191018073208'),
('G20180920011911','M20191027045823'),
('G20180920011911','M20191030045051'),
('G20180920011911','M20191105025201'),
('G20180920011911','M20191130100340'),
('G20180920011911','M20191130100341'),
('G20180920011911','M20191130100444'),
('G20180920011911','M20191130100514'),
('G20180920011911','M20191223120406'),
('G20180920011911','M20191223120425'),
('G20180920011911','M20200917104159'),
('G20180920011911','M20221008105527'),
('G20180920011911','M20221008105550'),
('G20180920011911','M20221008105551'),
('G20191009120544','M20191018073128'),
('G20191009120544','M20191018073208'),
('G20191009120544','M20200917104159'),
('G20191010114036','M20191027045823'),
('G20191010114036','M20191030045051'),
('G20191010114036','M20191105025201'),
('G20191013080855','M20180920031531'),
('G20191013080855','M20181222032707'),
('G20191013080855','M20191013080517'),
('G20191013080855','M20191017023845'),
('G20191013080855','M20191017023846'),
('G20191013080855','M20191130100444'),
('G20191013080855','M20191223120425'),
('G20191013080855','M20221008105527'),
('G20191013080855','M20221008105550'),
('G20221007093248','M20181222032707'),
('G20221007093248','M20191018064607'),
('G20221007093248','M20191018073146'),
('G20221007093248','M20221008105527'),
('G20221007093248','M20221008105550'),
('G20221007093333','M20191027045823'),
('G20221007093333','M20191030045051'),
('G20221007093333','M20191105025201'),
('G20221007093405','M20221008105551'),
('G20221007093416','M20191018064607'),
('G20221007093416','M20191018073146'),
('G20221007093416','M20191027045823'),
('G20221007093416','M20191030045051'),
('G20221007093416','M20191105025201'),
('G20221007093416','M20221008105551'),
('G20221007093431','M20191018064607'),
('G20221007093431','M20191027045823'),
('G20221007093431','M20191030045051'),
('G20221007093431','M20191105025201'),
('G20221007093444','M20181222032707'),
('G20221007093444','M20221008105550'),
('G20221007093501','M20181222032707'),
('G20221007093501','M20221008105527'),
('G20221007093517','M20191018073146'),
('G20221007093527','M20181222032707'),
('G20221007093527','M20191027045823'),
('G20221007093527','M20191030045051'),
('G20221007093527','M20191105025201'),
('G20221007093527','M20221008105527'),
('G20221007093527','M20221008105550'),
('G20221007093527','M20221008105551'),
('G20221007093536','M20181222032707'),
('G20221007093536','M20221008105527'),
('G20221007093536','M20221008105550'),
('G20221007093545','M20181222032707'),
('G20221007093545','M20191018073146'),
('G20221007093545','M20191027045823'),
('G20221007093545','M20191030045051'),
('G20221007093545','M20191105025201'),
('G20221007093545','M20221008105527'),
('G20221007093545','M20221008105550'),
('G20221007093545','M20221008105551'),
('G20221007093557','M20181222032707'),
('G20221007093557','M20221008105527'),
('G20221013064940','M20221008105551');

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `idgroup` char(15) NOT NULL,
  `group` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`idgroup`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `groups` */

insert  into `groups`(`idgroup`,`group`) values 
('G20180920011911','superadmin'),
('G20191009120544','donatur'),
('G20191010114036','pelatih'),
('G20191013080855','admin'),
('G20221007093248','pimpinan'),
('G20221007093305','pimpinan hrd'),
('G20221007093333','pimpinan sdm'),
('G20221007093405','pimpinan dept user'),
('G20221007093416','hrd'),
('G20221007093431','dept user'),
('G20221007093444','div penyaluran'),
('G20221007093501','tim manager program'),
('G20221007093517','spv manager'),
('G20221007093527','keuangan'),
('G20221007093536','accounting'),
('G20221007093545','pegawai'),
('G20221007093557','audit'),
('G20221013064940','payroll');

/*Table structure for table `kota` */

DROP TABLE IF EXISTS `kota`;

CREATE TABLE `kota` (
  `idkota` int(11) NOT NULL AUTO_INCREMENT,
  `kota` varchar(50) DEFAULT NULL,
  `st` char(1) DEFAULT NULL,
  PRIMARY KEY (`idkota`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `kota` */

insert  into `kota`(`idkota`,`kota`,`st`) values 
(1,'Surabaya','0'),
(2,'Sidoarjo','0'),
(3,'Gresik','0');

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `idmod` char(15) NOT NULL,
  `mod` varchar(30) DEFAULT NULL,
  `par` char(15) DEFAULT NULL,
  `urlpage` varchar(25) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `typ` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `modules` */

insert  into `modules`(`idmod`,`mod`,`par`,`urlpage`,`icon`,`typ`) values 
('M20180920031131','Home','-','dashboard','fa fa-desktop',0),
('M20180920031531','Manajemen User','-','users','fa fa-user',0),
('M20180920031644','Master','-','','fa fa-list',0),
('M20180920031751','Group User','M20180920031644','groups',NULL,3),
('M20180920032009','Modules','M20180920031644','modules',NULL,3),
('M20181222032707','Penyaluran','-','','fa fa-link',0),
('M20190817051952','Pengaturan','M20180920031644','setconfig',NULL,3),
('M20191013080517','Kota','M20180920031644','kota','fa fa-desktop',0),
('M20191013085041','Customer','M20180920031531','customer','',0),
('M20191017023845','Jenis Produk','M20180920031644','jenismenu','',0),
('M20191017023846','Produk','M20180920031644','daftarmenu','',0),
('M20191018064607','Rekrutmen','-','rekrutmen','fa fa-user-plus',0),
('M20191018073128','Pencatatan Bahan','M20191018064607','inventory','',2),
('M20191018073146','Penilaian','-','penilaian','fa fa-level-up',0),
('M20191018073208','Stock Gudang','M20191018064607','inventory/warehouse','',2),
('M20191026060041','Metode Pembayaran','M20180920031644','paymenttype','',3),
('M20191027045823','Pelatihan','-','','fa fa-graduation-cap',0),
('M20191030045051','Pelatihan Mandatory','M20191027045823','pelatihanm','',0),
('M20191105025201','Pelatihan Voluntary','M20191027045823','pelatihanv','',0),
('M20191130100340','Realisasi Penjualan','M20191027045823','inventory/rpenjualan','',3),
('M20191130100341','Overview Penjualan','M20191027045823','inventory/openjualan','',3),
('M20191130100444','Pemesanan','M20191027045823','report/reportpesan','',0),
('M20191130100514','Realisasi Pengeluaran Bahan','M20191027045823','inventory/rpengeluaran','',3),
('M20191223120406','Mutasi Bahan Baku','M20191027045823','inventory/mutasibb','',3),
('M20191223120425','Pembayaran','M20191027045823','report/reportbayar','',0),
('M20200917104159','Pencatatan Produk','M20191018064607','produkinventory','',2),
('M20210811082043','Users','M20180920031531','users','',0),
('M20221008105527','Bantuan','M20181222032707','bantuan','',0),
('M20221008105550','Bantuan Darurat','M20181222032707','bantuandarurat','',0),
('M20221008105551','Payroll','-','payroll','fa fa-money',0);

/*Table structure for table `sysconfig` */

DROP TABLE IF EXISTS `sysconfig`;

CREATE TABLE `sysconfig` (
  `idset` int(11) NOT NULL AUTO_INCREMENT,
  `nmsistem` varchar(15) DEFAULT NULL,
  `usn` varchar(50) DEFAULT NULL,
  `pwd` varchar(50) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `host` varchar(45) DEFAULT NULL,
  `smtp` varchar(10) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `credit` varchar(200) DEFAULT NULL,
  `ppn` int(11) DEFAULT NULL,
  `expired` date DEFAULT NULL,
  `maxusr` int(11) DEFAULT NULL,
  `maxbahan` int(11) DEFAULT NULL,
  `maxmenu` int(11) DEFAULT NULL,
  `maxoutlet` int(11) DEFAULT NULL,
  `lgbg` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idset`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `sysconfig` */

insert  into `sysconfig`(`idset`,`nmsistem`,`usn`,`pwd`,`port`,`host`,`smtp`,`icon`,`credit`,`ppn`,`expired`,`maxusr`,`maxbahan`,`maxmenu`,`maxoutlet`,`lgbg`) values 
(1,'SIM YM','wakanda008@gmail.com','tirus008untuk',587,'smtp.gmail.com','tls','ic1.jpeg','Developed by <a href=\"http://autotesis.com\" target=\"_blank\">Auto Teknomedia Sistem</a>',0,'2023-12-31',25,10,10,10,'bg1.jpg');

/*Table structure for table `t_log` */

DROP TABLE IF EXISTS `t_log`;

CREATE TABLE `t_log` (
  `idlog` int(11) NOT NULL AUTO_INCREMENT,
  `idusr` char(15) NOT NULL,
  `tableref` varchar(50) DEFAULT NULL,
  `idref` int(11) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idlog`,`idusr`),
  KEY `FK_log1` (`idusr`),
  CONSTRAINT `FK_log1` FOREIGN KEY (`idusr`) REFERENCES `usrs` (`idusr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_log` */

/*Table structure for table `t_payroll` */

DROP TABLE IF EXISTS `t_payroll`;

CREATE TABLE `t_payroll` (
  `idtrx` int(11) NOT NULL AUTO_INCREMENT,
  `idusr` char(15) NOT NULL,
  `tgl` datetime DEFAULT NULL,
  `periode` date DEFAULT NULL,
  `dok_lembur` varchar(50) DEFAULT NULL,
  `st_lembur` varchar(20) DEFAULT NULL,
  `dok_sip` varchar(50) DEFAULT NULL,
  `dok_peg` varchar(50) DEFAULT NULL,
  `st_dokpeg` varchar(20) DEFAULT NULL,
  `dok_bukti` varchar(50) DEFAULT NULL,
  `dok_slip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtrx`,`idusr`),
  KEY `FK_payroll1` (`idusr`),
  CONSTRAINT `FK_payroll1` FOREIGN KEY (`idusr`) REFERENCES `usrs` (`idusr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=833 DEFAULT CHARSET=latin1;

/*Data for the table `t_payroll` */

insert  into `t_payroll`(`idtrx`,`idusr`,`tgl`,`periode`,`dok_lembur`,`st_lembur`,`dok_sip`,`dok_peg`,`st_dokpeg`,`dok_bukti`,`dok_slip`) values 
(829,'U20200530052016','2022-10-14 23:01:36','2022-10-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(830,'U20200530052231','2022-10-14 23:01:44','2022-10-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(831,'U20210811051226','2022-10-15 21:45:53','2022-10-01',NULL,'Disetujui',NULL,NULL,NULL,NULL,NULL),
(832,'U20210811051226','2022-10-15 21:46:02','2022-09-01',NULL,'Ditolak','SIPU2021081105122620221015101627.pdf','KJPU2021081105122620221015104040.pdf','Selesai','PEMBU2021081105122620221015111340.pdf',NULL);

/*Table structure for table `t_pelamar` */

DROP TABLE IF EXISTS `t_pelamar`;

CREATE TABLE `t_pelamar` (
  `idpelamar` int(11) NOT NULL AUTO_INCREMENT,
  `idrekrut` int(11) NOT NULL,
  `nama` varchar(20) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `dok_pelamar` varchar(50) DEFAULT NULL,
  `st` varchar(20) DEFAULT NULL,
  `ket` varchar(50) DEFAULT NULL,
  `nilai` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idpelamar`,`idrekrut`),
  KEY `FK_pelamar1` (`idrekrut`),
  CONSTRAINT `FK_pelamar1` FOREIGN KEY (`idrekrut`) REFERENCES `t_rekrutmen` (`idrekrut`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `t_pelamar` */

insert  into `t_pelamar`(`idpelamar`,`idrekrut`,`nama`,`tgl`,`dok_pelamar`,`st`,`ket`,`nilai`) values 
(5,3,'Anton Wijaya','2022-10-11 18:22:20','PELAnton_Wijaya20221011062220.pdf','Tidak Lulus','',''),
(6,3,'Ririn Fauziah','2022-10-11 18:22:42','PELRirin_Fauziah20221011062242.pdf','Diterima','Tim Infrastruktur dan Jaringan','90');

/*Table structure for table `t_pelatihan_mandatory` */

DROP TABLE IF EXISTS `t_pelatihan_mandatory`;

CREATE TABLE `t_pelatihan_mandatory` (
  `idpm` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` datetime DEFAULT NULL,
  `nm` varchar(100) DEFAULT NULL,
  `dok_tna` varchar(50) DEFAULT NULL,
  `dok_proposal` varchar(50) DEFAULT NULL,
  `anggaran` int(11) DEFAULT NULL,
  `st` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idpm`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `t_pelatihan_mandatory` */

insert  into `t_pelatihan_mandatory`(`idpm`,`tgl`,`nm`,`dok_tna`,`dok_proposal`,`anggaran`,`st`) values 
(7,'2022-10-16 14:45:09','Pelatihan Peningkatan Softskill','TNAP20221016024509.pdf',NULL,NULL,'Ditolak'),
(8,'2022-10-16 15:06:09','Pelatihan Peningkatan K3','TNAP20221016030609.pdf',NULL,NULL,'Selesai');

/*Table structure for table `t_pelatihan_voluntary` */

DROP TABLE IF EXISTS `t_pelatihan_voluntary`;

CREATE TABLE `t_pelatihan_voluntary` (
  `idpv` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` datetime DEFAULT NULL,
  `jdl` varchar(100) DEFAULT NULL,
  `idusr` char(15) DEFAULT NULL,
  `dok_tna` varchar(50) DEFAULT NULL,
  `anggaran` int(11) DEFAULT NULL,
  `st` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idpv`),
  KEY `FK_pelatihan_v` (`idusr`),
  CONSTRAINT `FK_pelatihan_v` FOREIGN KEY (`idusr`) REFERENCES `usrs` (`idusr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `t_pelatihan_voluntary` */

insert  into `t_pelatihan_voluntary`(`idpv`,`tgl`,`jdl`,`idusr`,`dok_tna`,`anggaran`,`st`) values 
(10,'2022-10-17 10:09:30','Pelatihan Pembelajaran Ms Word','U20210811051226','TNAPU2022101510332020221017102908.pdf',NULL,'Selesai'),
(11,'2022-10-17 10:39:03','Pelatihan Membuat Presentasi Online','U20210811051226',NULL,NULL,'Ditolak');

/*Table structure for table `t_penerima_bantuan` */

DROP TABLE IF EXISTS `t_penerima_bantuan`;

CREATE TABLE `t_penerima_bantuan` (
  `idpb` int(11) NOT NULL AUTO_INCREMENT,
  `idpenyalur` int(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `dok_datadiri` varchar(50) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `st` varchar(50) DEFAULT NULL,
  `dok_sk` varchar(50) DEFAULT NULL,
  `dok_bantuan` varchar(50) DEFAULT NULL,
  `dok_lpj` varchar(50) DEFAULT NULL,
  `st_lpj` varchar(50) DEFAULT NULL,
  `dok_ba` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idpb`,`idpenyalur`),
  KEY `FK_pb1` (`idpenyalur`),
  CONSTRAINT `FK_pb1` FOREIGN KEY (`idpenyalur`) REFERENCES `t_penyaluran` (`idpenyalur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `t_penerima_bantuan` */

insert  into `t_penerima_bantuan`(`idpb`,`idpenyalur`,`nama`,`dok_datadiri`,`tgl`,`st`,`dok_sk`,`dok_bantuan`,`dok_lpj`,`st_lpj`,`dok_ba`) values 
(1,1,'Imam','DDImam20221010041619.pdf','2022-10-10 16:16:19','Wawancara','','','','',NULL),
(2,1,'Kamil','DDKamil20221010042242.pdf','2022-10-10 16:22:42','Penerima Bantuan','SKKamil20221010055145.pdf','DPKamil20221010055206.pdf','LPJKamil20221010070310.pdf','Verifikasi Accounting',NULL),
(3,1,'Inong','DDInong20221010052704.pdf','2022-10-10 16:52:21','Pengajuan','','','','',NULL),
(4,1,'Ali','DDAli20221010072258.pdf','2022-10-10 19:15:42','Penerima Bantuan','SKAli20221010074127.pdf','DPAli20221010074210.pdf','LPJAli20221010074347.pdf','Diterima',NULL),
(5,1,'Agung','DDAgung20221011112030.pdf','2022-10-11 11:20:30','Pengajuan','','','','',NULL),
(6,3,'Dusun ABC','DDDusun_ABC20221011115328.pdf','2022-10-11 11:53:28','Ditolak','','','','',NULL),
(7,3,'Baharudin','DDBaharudin20221011120355.pdf','2022-10-11 12:03:55','Penerima Bantuan','SKBaharudin20221011120641.pdf','DPBaharudin20221011010908.pdf','LPJBaharudin20221011013315.pdf','Diterima','BABaharudin20221011012659.pdf');

/*Table structure for table `t_penilaian` */

DROP TABLE IF EXISTS `t_penilaian`;

CREATE TABLE `t_penilaian` (
  `idpenilaian` int(11) NOT NULL AUTO_INCREMENT,
  `idusr` char(15) DEFAULT NULL,
  `tgl_ajukan` datetime DEFAULT NULL,
  `dok_hasilkinerja` varchar(50) DEFAULT NULL,
  `st` varchar(20) DEFAULT NULL,
  `dok_report` varchar(50) DEFAULT NULL,
  `hasileval` varchar(50) DEFAULT NULL,
  `dok_sk` int(11) DEFAULT NULL,
  `dok_sp` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idpenilaian`),
  KEY `FK_penilaian1` (`idusr`),
  CONSTRAINT `FK_penilaian1` FOREIGN KEY (`idusr`) REFERENCES `usrs` (`idusr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `t_penilaian` */

insert  into `t_penilaian`(`idpenilaian`,`idusr`,`tgl_ajukan`,`dok_hasilkinerja`,`st`,`dok_report`,`hasileval`,`dok_sk`,`dok_sp`) values 
(14,'U20210811051226','2022-10-12 18:50:25','DHKStaff_Pegawai20221012065025.pdf','Ditolak',NULL,NULL,NULL,NULL),
(16,'U20210811051226','2022-10-12 19:12:55','DHKStaff_Pegawai20221012071255.pdf','Perpanjangan Kontrak',NULL,'OK',0,NULL),
(17,'U20210811051226','2022-10-12 19:37:04','DHKStaff_Pegawai20221012073704.pdf','Pemutusan Kontrak',NULL,'Butuh pertimbangan',0,NULL);

/*Table structure for table `t_penyaluran` */

DROP TABLE IF EXISTS `t_penyaluran`;

CREATE TABLE `t_penyaluran` (
  `idpenyalur` int(11) NOT NULL AUTO_INCREMENT,
  `idusr` char(15) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `dok_indis` varchar(50) DEFAULT NULL,
  `jns` char(1) DEFAULT NULL,
  `st` char(2) DEFAULT NULL,
  `stket` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idpenyalur`,`idusr`),
  KEY `FK_penyalur1` (`idusr`),
  CONSTRAINT `FK_penyalur1` FOREIGN KEY (`idusr`) REFERENCES `usrs` (`idusr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `t_penyaluran` */

insert  into `t_penyaluran`(`idpenyalur`,`idusr`,`judul`,`tgl`,`dok_indis`,`jns`,`st`,`stket`) values 
(1,'U20180920042322','Bantuan Langsung Tanpa Perantara','2022-10-10 14:14:34','BR20221010021434.pdf','1','1','Tayang'),
(2,'U20200602083802','Bantuan Langsung Tanpa Diundi','2022-10-10 19:06:32','BR20221010070632.pdf','1','1','Tayang'),
(3,'U20180920042322','Bantuan Bencana Banjir','2022-10-11 11:39:36','-','2','1','Tayang'),
(4,'U20180920042322','Bantuan Longsor','2022-10-11 11:41:12','-','2','1','Tayang');

/*Table structure for table `t_peserta_pelatihan` */

DROP TABLE IF EXISTS `t_peserta_pelatihan`;

CREATE TABLE `t_peserta_pelatihan` (
  `idpeserta` int(11) NOT NULL AUTO_INCREMENT,
  `idpel` int(11) NOT NULL,
  `idusr` char(15) NOT NULL,
  `dok_hasil` varchar(50) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `jns` char(2) DEFAULT NULL,
  PRIMARY KEY (`idpeserta`,`idpel`,`idusr`),
  KEY `FK_daftarmenu` (`idusr`),
  KEY `FK_pm1` (`idpel`),
  CONSTRAINT `FK_pm2` FOREIGN KEY (`idusr`) REFERENCES `usrs` (`idusr`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=latin1;

/*Data for the table `t_peserta_pelatihan` */

insert  into `t_peserta_pelatihan`(`idpeserta`,`idpel`,`idusr`,`dok_hasil`,`tgl`,`jns`) values 
(141,8,'U20221008105054','HPAU2022100810505420221016035753.pdf','2022-10-16 15:57:53','pm'),
(142,8,'U20210811051226','HPAU2021081105122620221016040005.pdf','2022-10-16 16:00:05','pm'),
(144,10,'U20210811051226','HPAU2021081105122620221017105018.pdf','2022-10-17 10:50:18','pv'),
(145,10,'U20221008105054','HPAU2022100810505420221017105108.pdf','2022-10-17 10:51:08','pv');

/*Table structure for table `t_rekrutmen` */

DROP TABLE IF EXISTS `t_rekrutmen`;

CREATE TABLE `t_rekrutmen` (
  `idrekrut` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` datetime DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `dok_kebutuhan` varchar(50) DEFAULT NULL,
  `st` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idrekrut`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `t_rekrutmen` */

insert  into `t_rekrutmen`(`idrekrut`,`tgl`,`nama`,`dok_kebutuhan`,`st`) values 
(2,'2022-10-11 17:33:47','Staff HRD Junior','REK20221011053347.pdf','Cancel'),
(3,'2022-10-11 17:34:13','Senior IT','REK20221011053413.pdf','Disetujui');

/*Table structure for table `usrs` */

DROP TABLE IF EXISTS `usrs`;

CREATE TABLE `usrs` (
  `idusr` char(15) NOT NULL,
  `idgroup` char(15) NOT NULL,
  `nm` varchar(50) DEFAULT NULL,
  `un` varchar(50) DEFAULT NULL,
  `pwd` varchar(32) DEFAULT NULL,
  `phone` char(15) DEFAULT NULL,
  `createat` datetime DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  `st` char(1) DEFAULT NULL,
  PRIMARY KEY (`idusr`,`idgroup`),
  KEY `FK_usr` (`idgroup`),
  CONSTRAINT `FK_usr` FOREIGN KEY (`idgroup`) REFERENCES `groups` (`idgroup`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `usrs` */

insert  into `usrs`(`idusr`,`idgroup`,`nm`,`un`,`pwd`,`phone`,`createat`,`img`,`st`) values 
('U20180920042322','G20180920011911','Anapp','rootiku','b1fa065c7405792a3858b7a15bbdf027','087805580081','2019-01-01 00:00:00','U201809200423221.jpg','0'),
('U20200530052016','G20191013080855','Adminion','admin@gmail.com','21232f297a57a5a743894a0e4a801fc3','08123123131','2020-05-30 05:20:16','U20210811051226.jpg','0'),
('U20200530052231','G20221007093501','TMP','tmp@gmail.com','21232f297a57a5a743894a0e4a801fc3','0987654321','2020-05-30 05:22:31','U20200530052231.png','0'),
('U20200602083802','G20221007093248','Pimpinan','pimpinan@gmail.com','21232f297a57a5a743894a0e4a801fc3','087999899811','2020-06-02 08:38:02','U20200602083802.jpg','0'),
('U20210811051226','G20221007093545','Staff Pegawai','pegsatu@gmail.com','21232f297a57a5a743894a0e4a801fc3','0787777111123','2021-08-11 05:12:26','U20210811051226.jpg','0'),
('U20221008105054','G20221007093527','Keuangan','keuangan@gmail.com','21232f297a57a5a743894a0e4a801fc3','08098944441','2022-10-08 10:50:54','U20221008105054.jpg','0'),
('U20221008105126','G20221007093536','Accounting','accounting@gmail.com','21232f297a57a5a743894a0e4a801fc3','078089098078','2022-10-08 10:51:26','U20221008105126.jpg','0'),
('U20221008105157','G20221007093557','Audit','audit@gmail.com','21232f297a57a5a743894a0e4a801fc3','076542123112','2022-10-08 10:51:57','U20221008105157.jpg','0'),
('U20221008105708','G20221007093444','Penyaluran','penyaluran@gmail.com','21232f297a57a5a743894a0e4a801fc3','0786543219','2022-10-08 10:57:08','U20221008105708.jpg','0'),
('U20221011064020','G20221007093431','dept user','deptuser@gmail.com','21232f297a57a5a743894a0e4a801fc3','0981231231231','2022-10-11 06:40:20','U20221011064020.jpg','0'),
('U20221012064929','G20221007093517','spv manager','spvman@gmail.com','21232f297a57a5a743894a0e4a801fc3','08978887123123','2022-10-12 06:49:29','U20221012064929.jpg','0'),
('U20221013065001','G20221013064940','payroll','payroll@gmail.com','21232f297a57a5a743894a0e4a801fc3','086712312221123','2022-10-13 06:50:01','U20221013065001.jpg','0'),
('U20221015095642','G20221007093405','pimpinan dept user','pdu@gmail.com','21232f297a57a5a743894a0e4a801fc3','087678955231','2022-10-15 09:56:42','U20221015095642.jpg','0'),
('U20221015103320','G20221007093416','hrd','hrd@gmail.com','21232f297a57a5a743894a0e4a801fc3','08765423121','2022-10-15 10:33:20','U20221015103320.jpg','0'),
('U20221016030347','G20221007093333','pimpinan sdm','pimsdm@gmail.com','21232f297a57a5a743894a0e4a801fc3','08679123811231','2022-10-16 03:03:47','U20221016030347.jpg','0'),
('U20221016032330','G20191010114036','pelatih','pelatih@gmail.com','21232f297a57a5a743894a0e4a801fc3','0876671231231','2022-10-16 03:23:30','U20221016032330.jpg','0');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
